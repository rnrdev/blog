import os
import sys

class Formatter:
    @staticmethod
    def lpad(str, max_length):
        reqpads = 0 if max_length <= len(str) else max_length - len(str)
        return ' ' * reqpads + str 
    @staticmethod
    def rpad(str, max_length):
        reqpads = 0 if max_length <= len(str) else max_length - len(str)
        return str + ' ' * reqpads

class Logger:
    NONE = 0
    ERROR = 1
    WARNING = 2
    INFO = 3
    TRACE = 4
    def __init__(self, level = None):
        self.setLevel(level)
    def setLevel(self, level):
        if level < self.NONE or level > self.TRACE:
            return 
        self.level_ = level
    def error(self, msg):
        self.report(self.ERROR, msg)
    def warning(self, msg):
        self.report(self.WARNING, msg)
    def info(self, msg):
        self.report(self.INFO, msg)
    def trace(self, msg):
        self.report(self.TRACE, msg)
    def report(self, level, msg):
        if level < self.NONE or level > self.TRACE:
            return 
        if level > self.level_:
            return 
        print('[%s] %s' % (self.fmtlevel(level), msg))
    def fmtlevel(self, level):
        map = { self.NONE   : 'NONE', 
                self.ERROR  : 'ERROR', 
                self.WARNING: 'WARNING', 
                self.INFO   : 'INFO', 
                self.TRACE  : 'TRACE'
              }
        return map.get(level, '????')
    @staticmethod
    def levelFromString(str):
        map = { 'NONE'      : Logger.NONE, 
                'ERROR'     : Logger.ERROR, 
                'WARNING'   : Logger.WARNING, 
                'INFO'      : Logger.INFO, 
                'TRACE'     : Logger.TRACE
              }
        return map.get(str.upper(), None)
    def dump(self):
        print('logger:')
        print('  %s: %s' % (Formatter.rpad('level', 22), 
                            self.fmtlevel(self.level_)))
       
class Application:
    class Config:
        def __init__(self, argv):
            self.kv_ = []
            self.parse(argv)
        def parse(self, argv):
            for i in range(0, len(argv)):
                arg = argv[i]
                while True:
                    if len(arg) <= 2:
                        break
                    if arg[:2] != '--':
                        break
                    arg = arg[2:]
                    eqpos = arg.find('=')
                    if eqpos == -1:
                        k, v = arg, None
                    else:
                        k, v = arg[0:eqpos], arg[eqpos+1:]
                    self.kv_.append((k, v))
                    break
        def props(self):
            return self.kv_
        def dump(self):
            print('configuration:')
            for i in range(0, len(self.props())):
                k, v = self.props()[i]
                print('  %s: %s' % (Formatter.rpad(k, 22), v))

    class Program:
        def __init__(self, argv):
            self.workdir_ = os.getcwd()
            self.program_ = argv[0]
            self.argv_ = argv[1:]
        def dump(self):
            print('program:')
            print('  %s: %s' % (Formatter.rpad('working directory', 22), self.workdir_))
            print('  %s: %s' % (Formatter.rpad('program', 22), self.program_))

    def __init__(self, argv):
        self.logger_ = Logger(Logger.TRACE)
        self.program_ = self.Program(argv)
        self.config_ = self.Config(argv)
        
        level = Logger.INFO
        for kv in self.config_.props():
            if kv[0] == 'log-level':
                level = Logger.levelFromString(kv[1])
                break
        print 'level=', level
        self.logger_.setLevel(Logger.INFO if level is None else level)
            
    def dump(self):
        self.logger_.dump()
        self.program_.dump()
        self.config_.dump()
    def run(self):
        self.dump()
    
# camarie@CAMARIE-LPT C:\dev\blog\python\command-line    
# python code.py --log-level=trace --operation=copy --source=http://download.thinkbroadband.com/5MB.zip --destination-directory=.\data    
if __name__ == '__main__':
    app = Application(sys.argv)
    app.run()

    